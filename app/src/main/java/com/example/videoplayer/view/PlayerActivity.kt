package com.example.videoplayer.view

import android.os.Bundle
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.videoplayer.databinding.ActivityPlayerBinding
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource


class PlayerActivity : AppCompatActivity(){
    private lateinit var binding: ActivityPlayerBinding
    private var exoPlayer: ExoPlayer? = null
    private var playbackPosition = 0L
    private var forwardCount = 0L
    private var backwardCount = 0L
    private var playWhenReady = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setView()
        preparePlayer()
        setListeners()

        //actionbar
        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Player Screen"
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setView() {
        binding = ActivityPlayerBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    private fun preparePlayer() {
        exoPlayer = ExoPlayer.Builder(this).build()
        exoPlayer?.let { exoPlayer ->
            exoPlayer.playWhenReady = true
            binding.playerView.player = exoPlayer
            exoPlayer.setMediaSource(setMediaType())
            exoPlayer.seekTo(playbackPosition)
            exoPlayer.playWhenReady = playWhenReady
            exoPlayer.prepare()
        }
    }

    private fun setListeners() {
        exoPlayer!!.addListener(object : Player.Listener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)
                Log.e("TAG", "onPlayerStateChanged "+playbackState)
                if (playbackState == PlaybackStateCompat.STATE_FAST_FORWARDING) {
                    forwardCount+1
                    binding.tvForwardCount.text= forwardCount.toString()
                    //do something
                }
                if (playbackState == PlaybackStateCompat.STATE_REWINDING) {
                    backwardCount+1
                    binding.tvBackwardCount.text= backwardCount.toString()
                    //do something else
                }
            }

            override fun onPlaybackStateChanged(playbackState: Int) {
                Log.e("TAG", "onPlaybackStateChanged "+playbackState)
                if (playbackState == PlaybackStateCompat.STATE_FAST_FORWARDING) {
                    forwardCount+1
                    binding.tvForwardCount.text= forwardCount.toString()
                    //do something
                }
                if (playbackState == PlaybackStateCompat.STATE_REWINDING) {
                    backwardCount+1
                    binding.tvBackwardCount.text= backwardCount.toString()
                    //do something else
                }
                super.onPlaybackStateChanged(playbackState)
            }

            override fun onPlayWhenReadyChanged(playWhenReady: Boolean, reason: Int) {
                Log.e("TAG", "onPlayWhenReadyChanged "+reason)
                super.onPlayWhenReadyChanged(playWhenReady, reason)
            }
        })
    }

    private fun releasePlayer() {
        exoPlayer?.let { player ->
            playbackPosition = player.currentPosition
            playWhenReady = player.playWhenReady
            player.release()
            exoPlayer = null
        }
    }

    private fun setMediaType(): MediaSource {
        val defaultHttpDataSourceFactory = DefaultHttpDataSource.Factory()
        val mediaItem = MediaItem.fromUri(intent.getStringExtra("uri").toString())
        return DashMediaSource.Factory(defaultHttpDataSourceFactory)
            .createMediaSource(mediaItem)
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }
}