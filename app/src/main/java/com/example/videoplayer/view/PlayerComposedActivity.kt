package com.example.videoplayer.view

import android.annotation.SuppressLint
import android.media.metrics.PlaybackStateEvent.STATE_ENDED
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.example.videoplayer.R
import com.example.videoplayer.view.ui.theme.Purple200
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.firebase.analytics.FirebaseAnalytics
import java.util.concurrent.TimeUnit

class PlayerComposedActivity : ComponentActivity() {
    private var exoPlayer: ExoPlayer? = null
    private var forwardCount = mutableStateOf(0L)
    private var backwardCount = mutableStateOf(0L)
    private var pauseCountState = mutableStateOf(0L)
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        setContent {
            MainContent()
        }
    }

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    @Composable
    fun MainContent() {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text("Player Screen", color = Color.White) },
                    navigationIcon = {
                        IconButton(onClick = {
                            releasePlayer()
                            finish() }) {
                            Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = "Back"
                            )
                        }
                    },
                )
            },
            content = { MyContent() }
        )
    }

    @Composable
    fun MyContent(modifier: Modifier = Modifier) {

        Column(
            Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {

            // Fetching the Local Context
            val mContext = LocalContext.current

            // Declaring ExoPlayer
            exoPlayer = ExoPlayer.Builder(mContext).apply {
                setSeekBackIncrementMs(PLAYER_SEEK_BACK_INCREMENT)
                setSeekForwardIncrementMs(PLAYER_SEEK_FORWARD_INCREMENT)
            }.build()
            exoPlayer?.let { exoPlayer ->
                exoPlayer.playWhenReady = true
                exoPlayer.setMediaSource(setMediaType())
                exoPlayer.seekTo(0L)
                exoPlayer.playWhenReady = false
                exoPlayer.prepare()
            }

            var shouldShowControls by remember { mutableStateOf(false) }

            var isPlaying by remember { mutableStateOf(exoPlayer!!.isPlaying) }

            var totalDuration by remember { mutableStateOf(0L) }

            var currentTime by remember { mutableStateOf(0L) }

            var bufferedPercentage by remember { mutableStateOf(0) }

            var playbackState by remember { mutableStateOf(exoPlayer!!.playbackState) }

            Box(modifier = modifier) {
                DisposableEffect(key1 = Unit) {
                    val listener =
                        object : Player.Listener {
                            override fun onEvents(
                                player: Player,
                                events: Player.Events
                            ) {
                                super.onEvents(player, events)
                                totalDuration = player.duration.coerceAtLeast(0L)
                                currentTime = player.currentPosition.coerceAtLeast(0L)
                                bufferedPercentage = player.bufferedPercentage
                                isPlaying = player.isPlaying
                                playbackState = player.playbackState
                            }
                        }

                    exoPlayer!!.addListener(listener)

                    onDispose {
                        exoPlayer!!.removeListener(listener)
                        exoPlayer!!.release()
                    }
                }

                AndroidView(
                    modifier =
                    Modifier
                        .clickable {
                            shouldShowControls = shouldShowControls.not()
                        }
                        .fillMaxWidth()
                        .height(240.dp),
                    factory = {
                        StyledPlayerView(mContext).apply {
                            player = exoPlayer
                            useController = false

                        }
                    }
                )

                PlayerControls(
                    modifier = Modifier.fillMaxWidth(),
                    isVisible = { shouldShowControls },
                    isPlaying = { isPlaying },
                    title = { exoPlayer!!.mediaMetadata.displayTitle.toString() },
                    playbackState = { playbackState },
                    onReplayClick = {
                        exoPlayer!!.seekBack()
                        backwardCount.value = backwardCount.value + 1
                        sendAnalytics("Backward")
                    },
                    onForwardClick = {
                        exoPlayer!!.seekForward()
                        forwardCount.value = forwardCount.value + 1
                        sendAnalytics("Forward")
                    },
                    onPauseToggle = {
                        when {
                            exoPlayer!!.isPlaying -> {
                                sendAnalytics("Pause")
                                // pause the video
                                exoPlayer!!.pause()
                                pauseCountState.value = pauseCountState.value + 1
                            }
                            exoPlayer!!.isPlaying.not() &&
                                    playbackState == STATE_ENDED -> {
                                exoPlayer!!.seekTo(0)
                                exoPlayer!!.playWhenReady = true
                            }
                            else -> {
                                // play the video
                                // it's already paused
                                exoPlayer!!.play()
                                sendAnalytics("Play")
                            }
                        }
                        isPlaying = isPlaying.not()
                    },
                    totalDuration = { totalDuration },
                    currentTime = { currentTime },
                    bufferedPercentage = { bufferedPercentage },
                    onSeekChanged = { timeMs: Float ->
                        exoPlayer!!.seekTo(timeMs.toLong())
                    }
                )
            }

            SqareBox(shape = RectangleShape, modifier)



        }
    }
    @Composable
    fun SqareBox(shape: Shape, modifier: Modifier){
        Column(modifier = Modifier.fillMaxWidth().wrapContentSize(Alignment.Center).padding(top = 50.dp)) {
            Box(
                modifier = Modifier.size(200.dp).clip(shape).background(Color.White).border(2.dp,
                    Color.Black)
            ){
                Column(
                    Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.Start,
                    verticalArrangement = Arrangement.SpaceEvenly
                ) {
                    Row(modifier = modifier, horizontalArrangement = Arrangement.SpaceEvenly) {
                        Text(text = "Pause count: ", Modifier.padding(start = 10.dp))
                        Text(text = pauseCountState.value.toString())
                    }

                    Row(modifier = modifier, horizontalArrangement = Arrangement.SpaceEvenly) {
                        Text(text = "Forward count: ", Modifier.padding(start = 10.dp))
                        Text(text = forwardCount.value.toString())
                    }

                    Row(modifier = modifier, horizontalArrangement = Arrangement.SpaceEvenly) {
                        Text(text = "Backward count: ", Modifier.padding(start = 10.dp))
                        Text(text = backwardCount.value.toString())
                    }
                }
            }
        }
    }
    // For displaying preview in
// the Android Studio IDE emulator
    @Preview(showBackground = true)
    @Composable
    fun DefaultPreview() {
        MainContent()
    }

    private fun setMediaType(): MediaSource {
        val defaultHttpDataSourceFactory = DefaultHttpDataSource.Factory()
        val mediaItem = MediaItem.fromUri(intent.getStringExtra("uri").toString())
        return DashMediaSource.Factory(defaultHttpDataSourceFactory)
            .createMediaSource(mediaItem)
    }

    @OptIn(ExperimentalAnimationApi::class)
    @Composable
    private fun PlayerControls(
        modifier: Modifier = Modifier,
        isVisible: () -> Boolean,
        isPlaying: () -> Boolean,
        title: () -> String,
        onReplayClick: () -> Unit,
        onForwardClick: () -> Unit,
        onPauseToggle: () -> Unit,
        totalDuration: () -> Long,
        currentTime: () -> Long,
        bufferedPercentage: () -> Int,
        playbackState: () -> Int,
        onSeekChanged: (timeMs: Float) -> Unit
    ) {

        val visible = remember(isVisible()) { isVisible() }

        AnimatedVisibility(
            modifier = modifier,
            visible = visible,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Box(
                modifier = Modifier
                    .background(Color.Black.copy(alpha = 0.6f))
                    .height(240.dp)
            ) {
                TopControl(
                    modifier = Modifier
                        .align(Alignment.TopStart)
                        .fillMaxWidth(),
                    title = title
                )


                CenterControls(
                    modifier = Modifier
                        .align(Alignment.Center)
                        .fillMaxWidth(),
                    isPlaying = isPlaying,
                    onReplayClick = onReplayClick,
                    onForwardClick = onForwardClick,
                    onPauseToggle = onPauseToggle,
                    playbackState = playbackState
                )

                BottomControls(
                    modifier =
                    Modifier
                        .align(Alignment.BottomCenter)
                        .fillMaxWidth()
                        .animateEnterExit(
                            enter =
                            slideInVertically(
                                initialOffsetY = { fullHeight: Int ->
                                    fullHeight
                                }
                            ),
                            exit =
                            slideOutVertically(
                                targetOffsetY = { fullHeight: Int ->
                                    fullHeight
                                }
                            )
                        ),
                    totalDuration = totalDuration,
                    currentTime = currentTime,
                    bufferedPercentage = bufferedPercentage,
                    onSeekChanged = onSeekChanged
                )

            }
        }
    }

    @Composable
    private fun TopControl(modifier: Modifier = Modifier, title: () -> String) {
        val videoTitle = intent.getStringExtra("name")

        Text(
            modifier = modifier.padding(16.dp),
            text = videoTitle.toString(),
            style = MaterialTheme.typography.h6,
            color = Purple200
        )
    }

    @Composable
    private fun CenterControls(
        modifier: Modifier = Modifier,
        isPlaying: () -> Boolean,
        playbackState: () -> Int,
        onReplayClick: () -> Unit,
        onPauseToggle: () -> Unit,
        onForwardClick: () -> Unit
    ) {
        val isVideoPlaying = remember(isPlaying()) { isPlaying() }

        val playerState = remember(playbackState()) { playbackState() }

        Row(modifier = modifier, horizontalArrangement = Arrangement.SpaceEvenly) {
            IconButton(modifier = Modifier.size(40.dp), onClick = onReplayClick) {
                Image(
                    modifier = Modifier.fillMaxSize(),
                    contentScale = ContentScale.Crop,
                    painter = painterResource(id = R.drawable.ic_baseline_replay_24),
                    contentDescription = "Replay 5 seconds"
                )
            }

            IconButton(modifier = Modifier.size(40.dp), onClick = onPauseToggle) {
                Image(
                    modifier = Modifier.fillMaxSize(),
                    contentScale = ContentScale.Crop,
                    painter =
                    when {
                        isVideoPlaying -> {
                            painterResource(id = R.drawable.ic_baseline_pause_24)
                        }
                        isVideoPlaying.not() && playerState == STATE_ENDED -> {
                            painterResource(id = R.drawable.ic_baseline_replay_24)
                        }
                        else -> {
                            painterResource(id = R.drawable.ic_baseline_play_arrow_24)
                        }
                    },
                    contentDescription = "Play/Pause"
                )
            }

            IconButton(modifier = Modifier.size(40.dp), onClick = onForwardClick) {
                Image(
                    modifier = Modifier.fillMaxSize(),
                    contentScale = ContentScale.Crop,
                    painter = painterResource(id = R.drawable.ic_baseline_forward_10_24),
                    contentDescription = "Forward 10 seconds"
                )
            }
        }
    }

    @Composable
    private fun BottomControls(
        modifier: Modifier = Modifier,
        totalDuration: () -> Long,
        currentTime: () -> Long,
        bufferedPercentage: () -> Int,
        onSeekChanged: (timeMs: Float) -> Unit
    ) {

        val duration = remember(totalDuration()) { totalDuration() }

        val videoTime = remember(currentTime()) { currentTime() }

        val buffer = remember(bufferedPercentage()) { bufferedPercentage() }

        Column(modifier = modifier.padding(top = 150.dp)) {
            Box(modifier = Modifier.fillMaxWidth()) {
                Slider(
                    value = buffer.toFloat(),
                    enabled = false,
                    onValueChange = { /*do nothing*/ },
                    valueRange = 0f..100f,
                    colors =
                    SliderDefaults.colors(
                        disabledThumbColor = Color.Transparent,
                        disabledActiveTrackColor = Color.Gray
                    )
                )

                Slider(
                    modifier = Modifier.fillMaxWidth(),
                    value = videoTime.toFloat(),
                    onValueChange = onSeekChanged,
                    valueRange = 0f..duration.toFloat(),
                    colors =
                    SliderDefaults.colors(
                        thumbColor = Purple200,
                        activeTickColor = Purple200
                    )
                )
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    text = duration.formatMinSec(),
                    color = Purple200
                )

                IconButton(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    onClick = {}
                ) {
                    Image(
                        contentScale = ContentScale.Crop,
                        painter = painterResource(id = R.drawable.ic_baseline_fullscreen_24),
                        contentDescription = "Enter/Exit fullscreen"
                    )
                }
            }
        }
    }

    fun Long.formatMinSec(): String {
        return if (this == 0L) {
            "..."
        } else {
            String.format(
                "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(this),
                TimeUnit.MILLISECONDS.toSeconds(this) -
                        TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(this)
                        )
            )
        }
    }


    companion object {
        private const val PLAYER_SEEK_BACK_INCREMENT = 5 * 1000L // 5 seconds
        private const val PLAYER_SEEK_FORWARD_INCREMENT = 10 * 1000L // 10 seconds
    }

    private fun releasePlayer() {
        exoPlayer?.let { player ->
            player.release()
            exoPlayer = null
        }
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }

    private fun sendAnalytics(event:String){
        val bundle = Bundle()
        bundle.putString("Event", event)
        firebaseAnalytics.logEvent("Player Screen", bundle)
    }
}