package com.example.videoplayer.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.videoplayer.adapters.VideoListAdapter
import com.example.videoplayer.databinding.ActivityMainBinding
import com.example.videoplayer.model.ItemsViewModel
import com.example.videoplayer.util.Utils
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setView()
        binding.rvMovieList.layoutManager = LinearLayoutManager(this)

        val adapter = VideoListAdapter(getVideoList()) {
            Log.e("TAG", "itemclicked: " )
            startActivity(Intent(this, PlayerComposedActivity::class.java).apply {
                putExtra("name", it.name)
                putExtra("uri", it.uri)
            })
        }

        // Setting the Adapter with the recyclerview
        binding.rvMovieList.adapter = adapter
    }

    private fun setView() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    private fun getVideoList(): List<ItemsViewModel> {
        val gson = GsonBuilder().create()
        return gson.fromJson<ArrayList<ItemsViewModel>>(Utils.getJsonDataFromAsset(this), object :
            TypeToken<ArrayList<ItemsViewModel>>(){}.type)
    }
}