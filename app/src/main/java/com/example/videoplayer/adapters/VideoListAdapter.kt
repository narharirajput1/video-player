package com.example.videoplayer.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.videoplayer.R
import com.example.videoplayer.model.ItemsViewModel
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource

class VideoListAdapter(private val mList: List<ItemsViewModel>, val clickListener: (ItemsViewModel) -> Unit) : RecyclerView.Adapter<VideoListAdapter.ViewHolder>() {
    private var exoPlayer: ExoPlayer? = null
    private var playbackPosition = 0L
    private var playWhenReady = false
    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the item view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)

        exoPlayer = ExoPlayer.Builder(parent.context).build()
        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val itemsViewModel = mList[position]

        // sets the url to the playerview from our itemHolder class
        exoPlayer?.let { exoPlayer ->
            holder.playerView.player = exoPlayer
            exoPlayer.setMediaSource(setMediaType(itemsViewModel))
            exoPlayer.seekTo(playbackPosition)
            exoPlayer.playWhenReady = playWhenReady
            exoPlayer.prepare()
        }

        // sets the text to the textview from our itemHolder class
        holder.textView.text = itemsViewModel.name

        holder.view.setOnClickListener { clickListener(itemsViewModel) }

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to playerview and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val playerView: StyledPlayerView = itemView.findViewById(R.id.playerView)
        val textView: TextView = itemView.findViewById(R.id.tv_name)
        val view: LinearLayout = itemView.findViewById(R.id.main_layout)
    }

    private fun setMediaType(itemsViewModel: ItemsViewModel): MediaSource {
        val defaultHttpDataSourceFactory = DefaultHttpDataSource.Factory()
        val mediaItem = MediaItem.fromUri(itemsViewModel.uri)
        return DashMediaSource.Factory(defaultHttpDataSourceFactory)
            .createMediaSource(mediaItem)
    }

}
